# CarCar

Team:

* Victoria Pratt - Sales microservice
* Samuel Dorismond - Service microservice

## Design

## Sales microservice

This microservice has an AutomobileVO that gets automobile data from the Inventory microservice via a poller. The SalesPerson model and the Customer model are used for creating and accessing sales person and customer data, respectively. All three of these models are accessed through the Sales model, which is used for retrieving and creating automobile sales records. These are then displayed in a list of all sales records, and also in the sales history of the employee who sold the automobile.

## Service microservice

the models in the service microservice show the properties of the service classes ie the elements of the making an appointment for getting your car serviced. integrating it with the inventory microservice, it should show the inventory of the item and if it was purchased within that store which will highlight that the customer is a "VIP" and should get priority service.
