import React, { useEffect, useState } from 'react';

function TechnicianForm() {

    // Create Submit Feature //
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = nameChange;
        data.employee_number = EmployeeId;
        console.log(data)

        const technicianUrl = 'http://localhost:8080/api/technician/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok){
            const newTechnician = await response.json();
            setNameChange('');
            setEmployeeId('');

        }


    }

    // Create useStates to convert input into usable elements //
    // const [Technician, setTechnician] = useState('');
    const [nameChange, setNameChange] = useState('');
    const [EmployeeId, setEmployeeId] = useState('');;

    const handleNameChange = (event) => {
        const value = event.target.value;
        setNameChange(value);
    }
    const handleEmployeeId = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }


    //Create a useState to input Technician into dropdown menu
    const [technicians, TechnicianList] = useState(['']);


    //Access database for Technician dropdown menu
    const fetchData = async () =>{
        const url = "http://localhost:8080/api/technician/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            TechnicianList(data.technician)

        }
    }

    useEffect(()=>{
        fetchData();
    }, []);

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Technician</h1>
            <form onSubmit = {handleSubmit} id="create-Technician-form">
              <div className="form-floating mb-3">
                <input onChange= {handleNameChange} value = {nameChange} placeholder="name" id="name" className="form-control"/>
                <label htmlFor="name">Please enter your name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange= {handleEmployeeId} value = {EmployeeId} placeholder="employee id" required type="text" name="employee id" id="employee id" className="form-control"/>
                <label htmlFor="employee id"> Please enter your employee id </label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}


export default TechnicianForm;
