import { useParams, Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

function AppointmentDetail() {

    const[appointment, setAppointment] = useState({})
    const {id} = useParams()

    const getData = async() =>{
        const url = `http://localhost:8080/api/appointments/${id}`
        const response = await fetch(url)

        if (response.ok){
            const data = await response.json()
            setAppointment(data.appointments)
        }
    }

    useEffect(()=>{
        getData()
    },[])

    const handleDelete = async () => {
        const url = `http://localhost:8080/api/appointments/${id}`
        const fetchConfigs = {
            method: "Delete",
            headers: {
                "content=type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfigs)
        const data = await response.json()
    }

    return(
        <>
                    <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Details</th>
                    <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>vin</td>
                        <td>{appointment.vin}</td>
                    </tr>
                    <tr>
                        <td>customerName</td>
                        <td>{appointment.customerName}</td>
                    </tr>
                    <tr>
                        <td>date</td>
                        <td>{appointment.date}</td>
                    </tr>
                    <tr>
                        <td>technician</td>
                        <td>{appointment.technician}</td>
                    </tr>
                    <tr>
                        <td>reason</td>
                        <td>{appointment.reason}</td>
                    </tr>
                </tbody>
            </table>
            <footer>
            <Link to='/appointments' className="btn btn-primary">Return to appointment List</Link>
            <button onClick={handleDelete} className="btn btn-danger">Delete appointment</button>
            </footer>
        </>

    )
}


export default AppointmentDetail
