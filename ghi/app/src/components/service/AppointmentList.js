import React, { useEffect, useState } from 'react';
import {Link} from 'react-router-dom'

function listAppointments(props){
    return(
        <table className="table table-striped">
        <thead>
            <tr>
            <th>appointment</th>
            <th>inventory</th>
            </tr>
        </thead>
        <tbody>
        {props.appointment.map(appointment => {
                return (
                    <tr key={appointment.id}>
                    <td><Link to ={`/appointments/${appointment.id}`}>{appointment.model_name}</Link></td>
                    <td>{appointment}</td>
                    </tr>
                    );
            })}
        </tbody>
        </table>
    )
}
export default listAppointments
