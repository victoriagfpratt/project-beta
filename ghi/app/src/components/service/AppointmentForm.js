import React, { useEffect, useState } from 'react';

function AppointmentForm() {

    // Create Submit Feature //
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.date = date;
        data.customer_name = customerName;
        data.technician_id = technician;
        data.reason = reason;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        console.log(fetchConfig)
        console.log(data)
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok){
            const newAppointment = await response.json();
            console.log(newAppointment);

            setVin('');
            setCustomerName('');
            setDate('');
            setTechnician('');
            setReason('');
        }


    }

    // Create useStates to convert input into usable elements //
    const [vin, setVin] = useState('');
    const [date, setDate] = useState('');
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');
    const [customerName, setCustomerName]= useState('');
    const [technicians, setTechnicians] = useState([]);

    const handleVin = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleCustomerName = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }
    const handleDate = (event) => {
        const value = event.target.value;
        setDate(value);
    }
    const handleTechnician = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }
    const handleReason = (event) => {
        const value = event.target.value;
        setReason(value);
    }

// how to get data within a FK and make drop down options along with lines 46, 71
    const getTechnicianData = async () =>{
        const technicianUrl = 'http://localhost:8080/api/technician/';
        const response = await fetch(technicianUrl)
        if (response.ok){
            const data = await response.json()
            console.log(data)
            setTechnicians(data.technicians)
        }
    }
    useEffect(() => {
        getTechnicianData()
    }, [])
    // ^will tell the function to load the data when rendered^

    //Create a useState to input appointment into dropdown menu
    //Access database for Appointments dropdown menu

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Appointment</h1>
            <form onSubmit = {handleSubmit} id="create-appointment-form">
              <div className="mb-3">
                <input onChange= {handleVin} value = {vin} placeholder="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Please enter the vin number</label>
              </div>
              <div className="mb-3">
                <input onChange= {handleCustomerName} value = {customerName} placeholder="customer name" required type="text" name="customer name" id="customer name" className="form-control"/>
                <label htmlFor="customer name"> Name </label>
              </div>
              <div className="mb-3">
                <input onChange= {handleDate} value = {date} placeholder="date" required type="date" name="date" id="date" className="form-control"/>
                <label htmlFor="date"> date </label>
              </div>
              <div className="mb-3">
                <select value={technician} onChange={handleTechnician} required name="technician" id="technician" className="form-select">
                    <option value="">Choose a Technician</option>
                    {technicians.map(technician => {
                        return (
                        <option key={technician.id} value={technician.id}>
                            {technician.name}
                        </option>
                        );
                    })}
                </select>
              </div>
              <div className="mb-3">
                <input onChange= {handleReason} value = {reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason for appointment </label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}


export default AppointmentForm;
