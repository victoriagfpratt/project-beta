import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentList from './service/AppointmentList';
import AppointmentDetail from './service/AppointmentDetail';
import AppointmentForm from './service/AppointmentForm';
import TechnicianForm from './service/TechnicianForm';
// import TechnicianDetail from './TechnicianDetail';
// import TechnicianList from './TechnicianList';
import SalesList from './sales/SalesList';
import SalesForm from './sales/SalesForm';
import SalesPersonHistory from './sales/SalesPersonHistory';

function App(data) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='/appointments'>
            <Route index element={<AppointmentList appointment={data.service} />} />
            <Route path=":id" element={<AppointmentDetail />} />
            <Route path="create" element={<AppointmentForm />} />
          </Route>
          <Route path="/technician">
            <Route path='create' element={<TechnicianForm />} />
          </Route> 
          <Route path="/sales/">
            <Route index element={<SalesList sales={data.sales} />} />
            <Route path="create" element={<SalesForm />} />
            <Route path="sales_person" element={<SalesPersonHistory />} />
          </Route>
        </Routes>
      </div >
    </BrowserRouter >
  )
}

export default App;
