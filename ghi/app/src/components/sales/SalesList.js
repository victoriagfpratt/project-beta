import { Link } from "react-router-dom"
import React, { useState, useEffect } from "react"

function SalesList() {

  const [sales, setSale] = useState([])
  
  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/")
    if (response.ok) {
      const data = await response.json()
      setSale(data.sales)
    }
  }


  useEffect(() => {
    getData()
  }, [])


  const handleDelete = async (event) => {
    const url = `http://localhost:8090/api/sales/${event.target.id}/`
    const fetchConfigs = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json"
      }
    }
    const resp = await fetch(url, fetchConfigs)
    const data = await resp.json()

    setSale(sales.filter(sale => String(sale.id) !== event.target.id))
  }


  return (
    <div>
      <div><Link to="/sales/create/">Create a sales record</Link></div>
      <div><Link to="/sales/sales_person/">View employee sales history</Link></div>
      <table className="table">
        <thead>
          <tr>
            <th>Sales Person</th>
            <th>Employee Number</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
            return (
              <tr key={sale.id}>
                <td>{sale.sales_person.name}</td>
                <td>{sale.sales_person.employee_number}</td>
                <td>{sale.customer.name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
                <td><button onClick={handleDelete} id={sale.id} className="btn btn-danger">Delete</button></td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}
export default SalesList