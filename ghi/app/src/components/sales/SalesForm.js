import { Link } from "react-router-dom"
import React, { useState, useEffect } from "react"

function SalesForm() {

    const [automobiles, setAutomobiles] = useState([])
    const [auto, setAuto] = useState("")
    const [sales_person, setSalesPerson] = useState([])
    const [employee, setEmployee] = useState("")
    const [customers, setCustomers] = useState([])
    const [customer, setCustomer] = useState("")
    const [price, setPrice] = useState("")

    const handleAutomobileChange = (event) => {
        const value = event.target.value
        setAuto(value)
    }

    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setEmployee(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handlePriceChange = (event) => {
        const value = event.target.value
        setPrice(value)
    }

    const getAutomobileData = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/")
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setAutomobiles(data.autos)
        }
    }

    useEffect(() => {
        getAutomobileData()
    }, [])

    const getSalesPersonData = async () => {
        const response = await fetch("http://localhost:8090/api/sales_person/")
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setSalesPerson(data.sales_person)
        }
    }

    useEffect(() => {
        getSalesPersonData()
    }, [])

    const getCustomerData = async () => {
        const response = await fetch("http://localhost:8090/api/customers/")
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setCustomers(data.customer)
        }
    }

    useEffect(() => {
        getCustomerData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.automobile = auto
        data.sales_person = employee
        data.customer = customer
        data.price = price
        console.log(data)

        const salesUrl = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        }

        const response = await fetch(salesUrl, fetchConfig)
        if (response.ok) {
            await response.json()

            setAuto("")
            setEmployee("")
            setCustomer("")
            setPrice("")
        }
    }

    return (
        <div className="row">
            <h1>Sales Record</h1>
            <form onSubmit={handleSubmit} id="create-sales-record">
                <div className="mb-3">
                    <select value={auto} onChange={handleAutomobileChange}>
                        <option value="">Choose an automobile</option>
                        {automobiles.map(auto =>{
                            return(
                                <option key={auto.id} value={auto.vin}>
                                    {auto.model.manufacturer.name} {auto.model.name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={employee} onChange={handleSalesPersonChange}>
                        <option value="">Choose a sales person</option>
                        {sales_person.map(employee =>{
                            return(
                                <option key={employee.id} value={employee.id}>
                                    {employee.name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={customer} onChange={handleCustomerChange}>
                        <option value="">Choose a customer</option>
                        {customers.map(customer =>{
                            return(
                                <option key={customer.id} value={customer.id}>
                                    {customer.name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input onChange= {handlePriceChange} value = {price} placeholder="price" required type="text" name="price" id="price" className="form-control"/>
                    <label htmlFor="price">Input Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            <Link to="/sales/">Return to sales list</Link>
        </div>
    )
}
export default SalesForm
