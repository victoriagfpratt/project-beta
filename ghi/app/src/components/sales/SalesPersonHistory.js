import { Link } from "react-router-dom"
import React, { useEffect, useState } from "react"


function SalesPersonHistory() {

    const [sales, setSales] = useState([])
    const [employees, setEmployees] = useState([])
    const [id, setID] = useState([])
    const [employeeChange, setEmployeeChange] = useState(false)


    const getData = async () => {
        const response = await fetch(`http://localhost:8090/api/sales/${id}`)
        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }


    const getEmployeesData = async () => {
        const response = await fetch("http://localhost:8090/api/sales_person")
        if (response.ok) {
            const data = await response.json()
            setEmployees(data.sales_person)
        }
    }


    const handleEmployeeChange = (e) => {
        setID(e.target.value)
        setEmployeeChange(true)
    }


    if (employeeChange) {
        getData()
        setEmployeeChange(false)
    }


    useEffect(() => {
        getEmployeesData()
    }, [])


    return (
        <div>
            <h1>Employee Sales History</h1>
            <div><Link to="/sales/">Return to all sales</Link></div>
            <select required className="form-select" onChange={handleEmployeeChange}>
                <option value="">Choose a sales person</option>
                {employees.map(employee => {
                    return (
                        <option key={employee.id} value={employee.id}>
                            {employee.name}
                        </option>
                    )
                })}
            </select>
            <table className="table">
                <thead>
                    <tr>
                        <th>Sales person</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.sales_person.name}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default SalesPersonHistory
