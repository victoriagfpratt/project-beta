from django.contrib import admin
from .models import AutomobileVO, SalesPerson, Customer, Sales

@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPerson(admin.ModelAdmin):
    pass

@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass

@admin.register(Sales)
class Sales(admin.ModelAdmin):
    pass