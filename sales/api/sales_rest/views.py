import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import *
from .encoders import *


@require_http_methods(["GET", "POST"])
def api_sales_person(request):

    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=content["sales_person"])
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET"])
def api_show_employee(request, pk):

    response = JsonResponse({"message": "No sales people found"})
    response.status_code = 404
    if request.method == "GET":
        try:
            sales = SalesPerson.objects.get(pk=pk)
            return JsonResponse(
                sales,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return response



@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_sales(request):

    if request.method == "GET":
        sales = Sales.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesDetailEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            sales_person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=sales_person_id)
            content["sales_person"] = sales_person

            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile

            sales = Sales.objects.create(**content)
            return JsonResponse(
                sales,
                encoder=SalesDetailEncoder,
                safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "Could not create the sales record"},
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def api_show_sales(request, pk):

    response = JsonResponse({"message": "No records found"})
    response.status_code = 404

    if request.method == "GET":
        try:
            sales = Sales.objects.get(pk=pk)
            return JsonResponse(
                sales,
                encoder=SalesDetailEncoder,
                safe=False,
            )
        except Sales.DoesNotExist:
            return response
    else:
        try:
            sales = Sales.objects.get(pk=pk)
            sales.delete()
            return JsonResponse(
                sales,
                encoder=SalesDetailEncoder,
                safe=False,
            )
        except Sales.DoesNotExist:
            return response
