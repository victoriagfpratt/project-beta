from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=500, unique=True)
    color = models.CharField(max_length=100)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.IntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.id})


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.PositiveBigIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.pk})


class Sales(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name = "automobile",
        on_delete=models.CASCADE,
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name = "sales_person",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name = "customer",
        on_delete=models.CASCADE,
    )

    price = models.IntegerField()
    
    def get_api_url(self):
        return reverse("api_show_sales", kwargs={"pk": self.pk})
