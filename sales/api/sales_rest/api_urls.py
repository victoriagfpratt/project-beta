from django.urls import path
from .views import *

urlpatterns = [
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>/", api_show_sales, name="api_show_sales"),
    path("sales_person/", api_sales_person, name="api_sales_person"),
    path("sales_person/<int:pk>/", api_show_employee, name="api_show_employee"),
    path("customers/", api_customer, name="api_customer"),
]
