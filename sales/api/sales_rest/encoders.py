from common.json import ModelEncoder
from .models import *


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "color",
        "year",
        "vin",
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]


class SalesDetailEncoder(ModelEncoder):
    model = Sales
    properties = [
        "automobile",
        "sales_person",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),        
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }
