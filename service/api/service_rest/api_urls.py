from django.urls import path

from .views import *

urlpatterns = [
    path ('appointments/', api_list_appointments, name='api_list_appointments'),
    path ('appointments/<str:vin>/', api_show_appointment, name='api_show_appointment'),
    path('technician/', api_list_technicians, name='api_list_technicians'),
    path('technician/<int:id>/', api_technician, name='api_technician'),
]
