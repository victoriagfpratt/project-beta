from common.json import ModelEncoder
from .models import *

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "color",
        "year",
        "vin",
        "model",
    ]
class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'name',
    ]
    encoders = {
        "automobile", AutomobileVOEncoder(),
    }
class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'vin',
        'customer_name',
        'date',
        'reason',
        'technician',
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'vin',
    ]



class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'name',
        'employee_number',
    ]
    encoders = {
        "automobile", AutomobileVOEncoder(),
    }
