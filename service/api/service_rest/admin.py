from django.contrib import admin

from .models import *

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass
@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    pass

@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    pass

# @admin.register(ScheduledVO)
# class ScheduledVOAdmin(admin.ModelAdmin):
    pass
