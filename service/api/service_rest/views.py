from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import *
from .encoders import *


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method =="GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )

        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "No appointment available"},
                status=404,
            )


@require_http_methods(["GET", "DELETE"])
def api_show_appointment(requests, vin):
    if requests.method =="GET":
        try:
            response = Appointment.objects.get(vin=vin)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "no appointment found with the provided vin number"}
            )
        return JsonResponse(
            {'appointment': response},
            encoder = AppointmentDetailEncoder,
        )
    else: #DELETE
        count, _ = Appointment.objects.filter(vin=vin).delete()
        return JsonResponse({"appointment deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {'technicians': technicians},
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET"])
def api_technician(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    else: #delete
            count,_ = Technician.objects.filter(id=id).delete()
            return JsonResponse(
                {"technician was deleted successfully": count > 0}
            )
