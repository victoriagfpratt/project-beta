from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    import_href=models.CharField(max_length=50, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    model = models.CharField(max_length=50)

    def get_api_url(self):
        return reverse("api_automobileVO", kwargs={"id": self.id})

class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_technician", kwargs={"id": self.id})

class Appointment(models.Model):
    vin = models.CharField(max_length=50)
    customer_name = models.CharField(max_length=50)
    date = models.DateTimeField(auto_now=False, auto_now_add=False)
    technician = models.ForeignKey(
        Technician,
        verbose_name="technician",
        on_delete=models.CASCADE,
    )
    reason = models.CharField( max_length=200)

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"id": self.id})
